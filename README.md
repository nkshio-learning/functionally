## MEDIUM CLONE

This application is created as an assessment assignment for [functionally][1]. It displays a list of posts, and lets users to comment on each post. It uses a fake backend created using [json-server][2], and [jsonwebtoken][3]

**Login credentials**

```
1. Marty McFly
username: marty
password: password

2. Ankush Mehta
username: nkshio
password: password
```

**Available Features**

- [x] Load the posts from a _fake_ server

* [x] User authentication, and session management

- [x] Add a response on a post

* [x] Tested on Firefox, Chrome & Safari

- [x] Code Comments / Documentation

The following features have not been implemented due to lack of time, but are essential for the _production_ version of app.

- [ ] Add a new post

* [ ] Search for a post

- [ ] Local data persistence, caching

* [ ] Unit & Integration tests

- [ ] Semantic/UI improvements.

## Available Scripts

### Server

This project uses [json-server](<[https://github.com/typicode/json-server](https://github.com/typicode/json-server)>>) to build a fake REST API using `/server/database.json`

Inside the `/server` folder, run the following commands

- `npm install`

* `node server.js`

### PS

A server instance is live at `http://134.209.92.159:3000/`

### Client

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Inside the `/client` folder, you can run the following commands

- `npm start` - Runs the app in the development mode.

* `npm test` - Launches the test runner in the interactive watch mode.

- `npm run build`- Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance. The build is minified and the filenames include the hashes.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### PS

A client instance is running at `http://functionally-medium.herokuapp.com/`

[1]: https://www.functionally.com/
[2]: https://github.com/typicode/json-server
[3]: https://www.npmjs.com/package/jsonwebtoken
