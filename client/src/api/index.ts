import axios from 'axios';
import { REST_APIS } from '../lib/constants';
import { getToken } from '../lib/utils';
import { IResponseProps, ILoginProps } from '../lib/types';

/*
 * Wrapper function to make a GET call using axios
 */
const get = async (uri: string) => {
  try {
    const res = await axios.get(uri);
    return res.data;
  } catch (err) {
    throw new Error(err);
  }
};

/*
 * Wrapper function to make a POST call using axios
 */
const post = async (uri: string, data: any, headers?: any) => {
  try {
    const res = await axios.post(uri, data, headers);
    return res.data;
  } catch (err) {
    throw new Error(err);
  }
};

/*
 * function to login a user
 */
const login = (data: ILoginProps) => {
  return post(`${REST_APIS.ROOT}${REST_APIS.LOGIN}`, data);
};

/*
 * function to fetch all the posts with embeded responses
 */
const fetchPosts = () => {
  return get(
    `${REST_APIS.ROOT}${REST_APIS.POSTS}?_embed=${REST_APIS.RESPONSE}`
  );
};

/*
 * function to post response on a post
 */
const postResponse = (data: IResponseProps) => {
  return post(
    `${REST_APIS.ROOT}${REST_APIS.POSTS}/${data.postId}/${REST_APIS.RESPONSE}`,
    data,
    {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }
  );
};

export { login, fetchPosts, postResponse };
