export interface ILoginProps {
  username: string;
  password: string;
}

export interface IUserProps {
  token?: string;
  username?: string;
  thumbnail?: string;
}

interface IPost {
  body: string;
  title: string;
  author: string;
  claps: number;
  tags: string[];
  responses: IResponseProps[];
}

export interface IPostProps extends Partial<IPost> {
  id: string;
}

export interface IResponseProps {
  id?: number;
  body: string;
  postId: string;
  username?: string;
  thumbnail?: string;
}
