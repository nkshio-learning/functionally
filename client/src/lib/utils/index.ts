import { IUserProps } from '../types';

/*
 * Returns true if token, username, thumbnail exists on local-storage
 */
const isAuthenticated = () => {
  const token = getToken();
  const username = getUsername();
  const thumbnail = getThumbnail();

  if (token && username && thumbnail) return true;

  return false;
};

/*
 * Sets token, username, thumbnail in local-storage for current user
 */
const setUser = (user: IUserProps) => {
  try {
    const { token, username, thumbnail } = user;

    if (token && username && thumbnail) {
      localStorage.setItem('token', token);
      localStorage.setItem('username', username);
      localStorage.setItem('thumbnail', thumbnail);
      return true;
    }

    return false;
  } catch {
    return false;
  }
};

/*
 * Returns token from local-storage for current session
 */
const getToken = () => {
  const token = localStorage.getItem('token');

  if (token) return token;
  return undefined;
};

/*
 * Returns username from local-storage for current session
 */
const getUsername = () => {
  const username = localStorage.getItem('username');

  if (username) return username;
  return undefined;
};

/*
 * Returns thumbnail from local-storage for current session
 */
const getThumbnail = () => {
  const thumbnail = localStorage.getItem('thumbnail');

  if (thumbnail) return thumbnail;
  return undefined;
};

export { isAuthenticated, setUser, getToken, getUsername, getThumbnail };
