export const REST_APIS = {
  ROOT: 'http://134.209.92.159:3000/',
  POSTS: 'posts',
  LOGIN: 'auth/login',
  RESPONSE: 'responses',
};

export const TEXT = {
  ERROR: {
    LOGIN: 'Incorrect username or password',
    FETCH_POSTS: 'Error in fetching posts',
    SAVE_RESPONSE: 'Error in saving response',
  },
  SUCCESS: {
    SAVE_RESPONSE: 'Thanks for your response',
  },
  LABELS: {
    USERNAME: 'Username',
    PASSWORD: 'Password',
    RESPONSE: 'Write A Response...',
  },
  HEADING: {
    RESPONSES: 'Responses',
    LOGIN: 'Login to continue',
  },
  BUTTON: {
    LOGIN: 'Login',
  },
};
