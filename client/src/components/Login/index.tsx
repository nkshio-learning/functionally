import React from 'react';
import { Modal, Form, Input, message } from 'antd';
import { login } from '../../api';
import { TEXT } from '../../lib/constants';
import { isAuthenticated, setUser } from '../../lib/utils';
import './Login.scss';

interface IProps {
  visible: boolean;
}

interface IState {
  username: string;
  password: string;
  visible: boolean;
  confirmLoading: boolean;
}

class Login extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      username: '',
      password: '',
      visible: true,
      confirmLoading: false,
    };

    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleUsername = this.handleUsername.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
  }

  componentDidMount() {
    if (isAuthenticated()) {
      this.setState({ visible: false });
    }
  }

  componentWillReceiveProps() {
    const prevVisibility = this.state.visible;
    const newVisibility = this.props.visible;

    if (prevVisibility !== newVisibility) {
      this.setState({ visible: newVisibility });
    }
  }

  handleUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ username: event.target.value });
  };

  handlePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ password: event.target.value });
  };

  handleOk = () => {
    const { username, password } = this.state;
    const options = {
      username: username,
      password: password,
    };

    this.setState({
      confirmLoading: true,
    });

    login(options).then(
      (response) => {
        this.setState({
          visible: false,
          confirmLoading: false,
        });

        setUser(response);
      },
      (error) => {
        this.setState({
          confirmLoading: false,
        });

        message.error(TEXT.ERROR.LOGIN);
      }
    );
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const { visible, confirmLoading } = this.state;

    return (
      <Modal
        visible={visible}
        onOk={this.handleOk}
        okText={TEXT.BUTTON.LOGIN}
        title={TEXT.HEADING.LOGIN}
        onCancel={this.handleCancel}
        confirmLoading={confirmLoading}
      >
        <Form layout='vertical'>
          <Form.Item label={TEXT.LABELS.USERNAME}>
            <Input
              autoFocus
              value={this.state.username}
              onChange={this.handleUsername}
            />
          </Form.Item>

          <Form.Item label={TEXT.LABELS.PASSWORD}>
            <Input
              type='password'
              value={this.state.password}
              onPressEnter={this.handleOk}
              onChange={this.handlePassword}
            />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default Login;
