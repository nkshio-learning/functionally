import React from 'react';
import { Col, message } from 'antd';
import { IPostProps } from '../../lib/types';
import { TEXT } from '../../lib/constants';
import { isAuthenticated } from '../../lib/utils';
import { fetchPosts } from '../../api';
import List from '../List';
import Login from '../Login';
import './App.scss';

interface IState {
  posts: IPostProps[];
}

class App extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      posts: [],
    };
  }

  componentDidMount() {
    fetchPosts().then(
      (posts: IPostProps[]) => {
        this.setState({
          posts,
        });
      },
      (err) => {
        message.error(TEXT.ERROR.FETCH_POSTS);
      }
    );
  }

  render() {
    const { posts } = this.state;

    return (
      <div>
        {!isAuthenticated() ? <Login visible={true} /> : null}
        <Col span={12} offset={6}>
          <List posts={posts} />
        </Col>
      </div>
    );
  }
}

export default App;
