import React from 'react';
import { IPostProps } from '../../lib/types';
import Post from '../Post';
import './List.scss';

interface IProps {
  posts: IPostProps[];
}

const List: React.FC<IProps> = ({ posts = [] }) => {
  return (
    <div>
      {posts.map((post) => {
        return <Post key={post.id} data={post} />;
      })}
    </div>
  );
};

export default List;
