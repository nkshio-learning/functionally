import React from 'react';
import { message, Input } from 'antd';
import { postResponse } from '../../api';
import { TEXT } from '../../lib/constants';
import { IPostProps } from '../../lib/types';
import applause from '../../assets/icons/applause.png';
import { isAuthenticated, getUsername, getThumbnail } from '../../lib/utils';
import Login from '../Login';
import './Post.scss';

interface IProps {
  data: IPostProps;
}

interface IState {
  data: IPostProps;
  response: string;
  showLogin: boolean;
}

class Post extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      response: '',
      showLogin: false,
      data: this.props.data,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleResponse = this.handleResponse.bind(this);
  }

  handleResponse = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ response: event.target.value, showLogin: false });
  };

  handleSubmit = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (!isAuthenticated()) this.setState({ showLogin: true });
    else if (this.state.response !== '') {
      const response = {
        username: getUsername(),
        thumbnail: getThumbnail(),
        body: this.state.response,
        postId: this.props.data.id,
      };

      postResponse(response).then(
        () => {
          const { data } = this.state;
          if (data.responses) data.responses.push(response);
          else data.responses = [response];

          this.setState({ response: '' });
          message.success(TEXT.SUCCESS.SAVE_RESPONSE);
        },
        () => {
          message.error(TEXT.ERROR.SAVE_RESPONSE);
        }
      );
    }
  };

  render() {
    const { data, showLogin } = this.state;

    return (
      <div className='post'>
        <div className='title'>{data.title}</div>
        <div className='body'>{data.body}</div>
        <div className='tags'>
          {data.tags &&
            data.tags.map((tag, index) => {
              return (
                <div className='tag' key={index}>
                  {tag}
                </div>
              );
            })}
        </div>
        <div className='claps'>
          <div>
            <img className='icon' src={applause} alt='Claps' />
          </div>
          <div className='count'>{data.claps}</div>
        </div>
        <div className='responses'>
          <div className='heading'>{TEXT.HEADING.RESPONSES}</div>
          <Input
            allowClear
            value={this.state.response}
            onChange={this.handleResponse}
            onPressEnter={this.handleSubmit}
            placeholder={TEXT.LABELS.RESPONSE}
          />
          <div className='list'>
            {data.responses &&
              data.responses.map((response) => {
                return (
                  <div className='response' key={response.body}>
                    <div className='user'>
                      <img
                        alt='thumbnail'
                        className='thumbnail'
                        src={response.thumbnail}
                      />
                      <div className='username'>{response.username}</div>
                    </div>
                    <div className='content'>{response.body}</div>
                  </div>
                );
              })}
          </div>
        </div>
        {!isAuthenticated() && showLogin ? <Login visible={showLogin} /> : null}
      </div>
    );
  }
}

export default Post;
