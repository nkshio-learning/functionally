import React from 'react';
import ReactDOM from 'react-dom';
import Post from '.';

it('renders without crashing', () => {
  const post = {
    id: '1',
    title: 'First',
    body:
      "Yeah, but I never picked a fight in my entire life. I just wanna use the phone. I'm gonna ram him. I'm writing this down, this is good stuff. Stop it.",
    claps: 0,
    tags: ['Doc', 'Marty McFly', 'Time Travel'],
    author: 'Ankush Mehta',
  };
  const div = document.createElement('div');

  ReactDOM.render(<Post data={post} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
